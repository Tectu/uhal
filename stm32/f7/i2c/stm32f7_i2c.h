/*
 * Copyright (c) 2015 Joel Bodenmann <joel@seriouslyembedded.com>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgement in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

/**
 * @file   stm32f7_i2c.h
 * @author Joel Bodenmann <joel@seriouslyembedded.com>
 * @date   July, 20015
 * @brief  STM32F7xx I2C HAL using polling
 *
 * @defgroup I2C I2C
 * @ingroup F7
 *
 * STM32F7xx I2C HAL
 *
 * Limitations:
 *     + No interrupts, polling only
 *     + Only 7-bit slave addresses supported, no support for 10-bit addresses
 *     + Maximum read- and write length 255 bytes per transfer
 *     + STM32 can only be master
 *
 * @{
 */

#pragma once

#include "stm32f7xx.h"

/**
 * @brief Initialze the I2Cx peripheral
 *
 * @param i2c The I2Cx peripheral handle
 *
 * @return TRUE on success, FALSE otherwise
 */
bool_t i2cInit(I2C_TypeDef* i2c);

/**
 * @brief Send bytes to a slave
 *
 * @note 0 < @p length <= 255
 *
 * @param i2c The I2Cx peripheral handle
 * @param slaveAddr The slave address (including the R/W bit)
 * @param data The buffer containing the bytes to be sent
 * @param length The length of the buffer (number of bytes)
 */
void i2cSend(I2C_TypeDef* i2c, uint8_t slaveAddr, uint8_t* data, uint16_t length);

/**
 * @brief Send one byte to a slave
 *
 * @param i2c The I2Cx peripheral handle
 * @param slaveAddr The slave address (including the R/W bit)
 * @param data The byte to be sent
 */
void i2cSendByte(I2C_TypeDef* i2c, uint8_t slaveAddr, uint8_t data);

/**
 * @brief Write a specific value to a specific register in a slave
 *
 * @param i2c The I2Cx peripheral handle
 * @param slaveAddr The slave address (including the R/W bit)
 * @param regAddr The address of the register
 * @param value The value to be written to the specified register
 */
void i2cWriteReg(I2C_TypeDef* i2c, uint8_t slaveAddr, uint8_t regAddr, uint8_t value);

/**
 * @brief Read bytes from a slave
 *
 * @note 0 < @p length <= 255
 *
 * @param i2c The I2Cx peripheral handle
 * @param slaveAddr The slave address (including the R/W bit)
 * @param data The buffer to which the data will be written to
 * @param length The length of the buffer (number of bytes)
 */
void i2cRead(I2C_TypeDef* i2c, uint8_t slaveAddr, uint8_t* data, uint16_t length);

/**
 * @brief Read the register content from a slave (one byte)
 *
 * @param i2c The I2Cx peripheral handle
 * @param slaveAddr The slave address (including the R/W bit)
 * @param regAddr The register which should be read
 *
 * @return The register content
 */
uint8_t i2cReadByte(I2C_TypeDef* i2c, uint8_t slaveAddr, uint8_t regAddr);

/**
 * @brief Read the register content from a slave (two bytes)
 *
 * @param i2c The I2Cx peripheral handle
 * @param slaveAddr The slave address (including the R/W bit)
 * @param regAddr The register which should be read
 *
 * @return The register content
 */
uint16_t i2cReadWord(I2C_TypeDef* i2c, uint8_t slaveAddr, uint8_t regAddr);

/** @} */
